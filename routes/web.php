<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

//ruta de recursos GET POST PUT DELETE
Route::resource('/registros',App\Http\Controllers\RegistrosController::class);
Route::resource('/tipos',App\Http\Controllers\TiposController::class);

//ruta normal 
//Route::get('/registros',[App\Http\Controllers\RegistrosController::class,'index']);