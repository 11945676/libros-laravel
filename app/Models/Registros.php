<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Registros extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = ['autor','titulo','descripcion','editorial','fecha','ventas','id_tipo'];
}
