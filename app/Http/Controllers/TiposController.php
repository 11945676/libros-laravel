<?php

namespace App\Http\Controllers;

use App\Models\Tipos;
use Illuminate\Http\Request;

class TiposController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $tipos = Tipos::all();
        return view('tipos', compact('tipos'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('crear');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'tipo'=> 'required'
        ]);
        $tipos = new Tipos($request->input());
        $tipos-> saveOrFail();
        return redirect()->route('tipos.index')->with('success','Género de libro guardado');
    }

    /**
     * Display the specified resource.
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $tipo= Tipos::find($id);
            return view('editarTipo',compact('tipo'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'tipo'=> 'required'
        ]);
        $tipos = Tipos::find($id);
        $tipos-> update($request->input());
        return redirect()->route('tipos.index')->with('success','Género de libro Actualizado');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $tipos = Tipos::find($id);
        $tipos->delete();
        return redirect()->route('tipos.index')->with('success','GÉNERO DE LIBRO ELIMINADO');
    }
}
