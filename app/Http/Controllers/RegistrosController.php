<?php

namespace App\Http\Controllers;

use App\Models\Tipos;
use App\Models\Registros;
use Illuminate\Http\Request;

class RegistrosController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $registros = Registros::select('registros.id','autor','titulo','descripcion','editorial','fecha','ventas','id_tipo','tipo')
        ->join('tipos','tipos.id','=','registros.id_tipo')->get();
        $tipos = Tipos::all();
        return view('librovista',compact('registros'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $tipos = Tipos::all();

        return view('crear', compact('tipos'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'autor'=> 'required',
            'titulo'=> 'required',
            'descripcion'=> 'required',
            'editorial'=> 'required',
            'fecha' => 'required',
            'ventas' => 'required|numeric',
            'id_tipo' => 'required',

        ]);
        $registros = new Registros($request->input());
        $registros-> save();
        return redirect()->route('registros.index')->with('success','Acción realizada exitosamente');
    }

    /**
     * Display the specified resource.
     */
    public function show(Registros $registros)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($id)
    {
        $registros= Registros ::find($id);
        $tipos = Tipos::all();
        return view('editar',compact('registros', 'tipos'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'autor'=> 'required',
            'titulo'=> 'required',
            'descripcion'=> 'required',
            'editorial'=> 'required',
            'fecha' => 'required',
            'ventas' => 'required|numeric',
            'id_tipo' => 'required',

        ]);
        
        $registro = Registros::find($id);
        $registro-> update($request->input());
        return redirect()->route('registros.index')->with('success',' Registro Actualizado');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($id)
    {
        $registros = Registros::find($id);
        $registros->delete();
        return redirect()->route('registros.index')->with('success','Registro eliminado');
    }
}
