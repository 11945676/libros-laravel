@extends('plantilla')
@section('titulo')
-Registros
@endsection

@section('principal')

@if($mensaje = Session::get('success'))
    <div class="row" id="divok">
        <div class="col-md-6 offset-md-3">
            <div class="alert alert-success">
                {{ $mensaje }}
            </div>
        </div>
    </div>
@endif

<div class="row mt-3">
    <div class="col-md-4 offset-md-4">
        <div class="d-grid mx-auto">
        <button class="btn btn-dark" data-bs-toggle="modal" data-bs-target="#modalTipos">
            <i class="fa-solid fa-circle-plus"></i>
           AÑADIR </button>
    </div>
</div>
</div>

<div class="row mt-3">
    <div class="col-12">
        <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>GÉNERO DE LIBRO</th>
                                <th>EDITAR</th>
                                <th>ELIMINAR</th>                                
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($tipos as $i => $row)
                                <tr>
                                    <td>{{($i+1)}}</td>
                                    <td>{{$row->tipo}}</td>
                                    <td>
                                        <a class="btn btn-warning" href="{{ route('tipos.edit',$row->id)}}"> <i class="fa-solid fa-pencil"></i> </a>
                                    </td>
                                     <td>   
                                        <form method="POST" id="frm_{{$row->id}}" action="{{route('tipos.destroy', $row->id)}}">
                                            @csrf
                                            @method('DELETE')
                                            <button  onclick="datos('{{$row->id}}','{{$row->tipo}}')" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#modalEliminar"><i class="fa-solid fa-trash-can" style="color: #f0f2f4;"></i></button>
                                        </form>
                                    </td>
                                    </td>

                                    
                                    
                                </tr>
                            @endforeach
                        </tbody>
                    </div>
        </div>
</div>

@if($errors->any())
  <div class="row">
    <div class="col-md-6 offset-md-3">
      <div class="alert alert-danger">
        <ul>
          @foreach($errors->all() as $e)
            <li>{{$e}} </li>
          @endforeach
        </ul>
      </div>
    </div>
  </div>
@endif

<div class="modal" tabindex="-1" id="modalTipos">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">GÉNERO DE LIBRO</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <form method="POST">
                @csrf 
                @yield('metodo')
                <div class="input-group mb-3">
                    <span class="input-group-text" ><i class="fa-solid fa-book"></i></span>
                    <input name="tipo" type="text" class="form-control" placeholder="GÉNERO DE LIBRO" @isset($tipo)
                        value = "{{$tipos->tipo}}"
                    @endisset>
                  </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"> <i class="fa-solid fa-ban"></i> Cancelar</button>
          <button class="btn btn-success"><i class="fa-solid fa-floppy-disk"></i> GUARDAR</button>
        </div>
      </div>
    </div>
  </div>



  <div class="modal" tabindex="-1" id="modalEliminar">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title"><i class="fa-solid fa-warning"></i> D A G E R ! ! !</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <p>¿Seguro de elimiar el género del libro ?</p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"> <i class="fa-solid fa-ban"></i> Cancelar</button>
          <button id="btnEliminar" type="button" class="btn btn-success">Si, eliminar</button>
        </div>
      </div>
    </div>
  </div>
@vite('resources/js/listado.js')
@endsection
