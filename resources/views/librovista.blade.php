@extends('plantilla')
@section('titulo')
-Registros
@endsection

@section('principal')

@if($mensaje = Session::get('success'))
    <div class="row" id="divok">
        <div class="col-md-6 offset-md-3">
            <div class="alert alert-success">
                {{ $mensaje }}
            </div>
        </div>
    </div>
@endif
<div class="row">
    <div class="col-12">
        <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>AUTOR</th>
                                <th>TITULO DEL LIBRO</th>
                                <th>DESCRIPCIÓN</th>
                                <th>EDITORIAL</th>
                                <th>FECHA</th>
                                <th>VENTAS</th>
                                <th>TIPOS</th>
                            
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($registros as $i => $row)
                                <tr>
                                    <td>{{($i+1)}}</td>
                                    <td>{{$row->autor}}</td>
                                    <td>{{$row->titulo}}</td>
                                    <td>{{$row->descripcion}}</td>
                                    <td>{{$row->editorial}}</td>
                                    <td>{{$row->fecha}}</td>
                                    <td>{{$row->ventas}}</td>
                                    <td>{{$row->tipo}}</td>
                                    <td>
                                        <a class="btn btn-warning" href="{{ route('registros.edit',$row->id)}}"> <i class="fa-solid fa-pencil"></i> </a>
                                    </td>
                                    <td>

                                        <form method="POST" id="frm_{{$row->id}}" action="{{route('registros.destroy', $row->id)}}">
                                            @csrf
                                            @method('DELETE')
                                            <button onclick="datos('{{$row->id}}','{{$row->autor}}')" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#modalEliminarTipo"><i class="fa-solid fa-trash-can" style="color: #f0f2f4;"></i></button>
                                        </form>
                                        
                                    </td>

                                    
                                    
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="modal" tabindex="-1" id="modalEliminarTipo">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title"><i class="fa-solid fa-warning"></i> Atención!!!</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                              <p>¿Seguro de elimiar el registro?</p>
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"> <i class="fa-solid fa-ban"></i> Cancelar</button>
                              <button id="btnEliminarR" type="button" class="btn btn-success">Si, eliminar</button>
                            </div>
                          </div>
                        </div>
                      </div>

                </div>
            </div>
        </div>
    </div>
</div>
</div>

@endsection

@section('js')
    @vite('resources/js/listado.js')
@endsection

