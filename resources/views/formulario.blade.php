
@section('principal')
@if($errors->any())
  <div class="row">
    <div class="col-md-6 offset-md-3">
      <div class="alert alert-danger">
        <ul>
          @foreach($errors->all() as $e)
            <li>{{$e}} </li>
          @endforeach
        </ul>
      </div>
    </div>
  </div>
@endif

<div class="row">
    <div class="col  mt-3">
        <div class="card">
            <div class="card-header"><h3>REGISTRO DE LIBRO<h3></div>
            <div class="card-body">
                    <form method="POST" @yield('action')>
                        @csrf 
                        @yield('metodo')
                        <div class="input-group mb-3">
                            <span class="input-group-text" ><i class="fa-solid fa-user"></i></span>
                            <input name="autor" type="text" class="form-control" placeholder="autor" @isset($registros)
                                value = "{{$registros->autor}}"
                            @endisset>
                          </div>
                          <div class="input-group mb-3">
                            <span class="input-group-text" ><i class="fa-solid fa-book "></i></span>
                            <input name="titulo" type="text" class="form-control" placeholder="titulo" @isset($registros)
                            value = "{{$registros->titulo}}"
                        @endisset>
                          </div>
                          <div class="input-group mb-3">
                            <span class="input-group-text" ><i class="fa-solid fa-book"></i></span>
                            <input name="descripcion" type="text" class="form-control" placeholder="descripcion" @isset($registros)
                            value = "{{$registros->descripcion}}"
                        @endisset>
                          </div>
                          <div class="input-group mb-3">
                            <span class="input-group-text" ><i class="fa-solid fa-book"></i></span>
                            <input name="editorial" type="text" class="form-control" placeholder="editorial" @isset($registros)
                            value = "{{$registros->editorial}}"
                        @endisset>
                          </div>
                          <div class="input-group mb-3">
                            <span class="input-group-text" ><i class="fa-solid fa-book"></i></span>
                            <input name="fecha" type="date" class="form-control" placeholder="fecha" @isset($registros)
                            value = "{{$registros->fecha}}"
                        @endisset>
                          </div>
                          <div class="input-group mb-3">
                            <span class="input-group-text" ><i class="fa-solid fa-book"></i></span>
                            <input name="ventas" type="number" class="form-control" placeholder="ventas" @isset($registros)
                            value = "{{$registros->ventas}}"
                        @endisset>
                          </div>
                          <div class="input-group mb-3">
                            <span class="input-group-text" ><i class="fa-solid fa-book"></i></span>
                            <select name="id_tipo" class="form-select" required>
                                    @foreach($tipos as $row)
                                   
                                    <option value="{{$row->id}}">{{$row->tipo}}</option>
                                     
                                    @endforeach
                            </select>
                          </div>
                          
                          <button class="btn btn-success">guardar</button>
                    </form>
            </div>
        </div>
    </div>
</div>
@vite('resources/js/listado.js')
@endsection