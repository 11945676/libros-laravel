@extends('plantilla')
@section('form')
Creando...
@endsection
@section('principal')
<div class="row mt-6">
    <div class="col-md-6 offset-md-3">
        <div class="card">
            <div class="card-header bg-dark text-white">Género de libro</div>
            <div class="card-body">
                <form method="POST"  action="{{ route('tipos.update',$tipo->id) }}" >
                    @csrf 
                    @method('PUT')
                    <div class="input-group mb-3">
                        <span class="input-group-text" ><i class="fa-solid fa-book"></i></span>
                        <input name="tipo" type="text" class="form-control" placeholder="GÉNERO DE LIBRO" @isset($tipo)
                            value = "{{$tipo->tipo}}" 
                        @endisset>
                      </div>
                     <div class="card-footer">
                        <div class="row-md-3 offset-md-3">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"> <i class="fa-solid fa-ban"></i> Cancelar</button>
                        <button class="btn btn-success"><i class="fa-solid fa-floppy-disk"></i> GUARDAR</button>    
                        </div>
                    </div>   
            </div>
        </div>

    </div>
    
</div>  
@endsection
